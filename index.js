console.log("Hello World")


let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100, 
	attack: 50, 
	tackle: function() { 
		console.log("This pokemon tackled targetPokemon")
		console.log("targetPokemon's health is not reduced to targetPokemonHealth")
						},
	faint: function() {
		console.log("Pokemon fainted.")
						}
}

console.log(myPokemon)

// Creating an object constructor
function Pokemon (name, level) {
	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level; 

	// Methods
	this.tackle = function(target) {
		let newHealth = (target.health - this.attack)
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + newHealth)
		delete target.health
		target.health = newHealth
	}, 
	
	this.faint = function(target) { 
		if (target.health <= 0)  {
			console.log(target.name + " fainted.")
		}
	}
}

let pikachu = new Pokemon ("Pikachu", 500)
let squirtle = new Pokemon ("Squirtle", 1000)

console.log(pikachu)
console.log(squirtle)

pikachu.tackle(squirtle);
pikachu.tackle(squirtle);
pikachu.tackle(squirtle);
pikachu.tackle(squirtle);
pikachu.tackle(squirtle);
pikachu.tackle(squirtle);
pikachu.faint(squirtle);
pikachu.tackle(squirtle);
pikachu.faint(squirtle);





